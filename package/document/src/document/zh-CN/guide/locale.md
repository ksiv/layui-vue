::: title 国际化
:::

::: describe 目前的默认文案是简体中文，如果需要使用其他语言，可以参考下面的方案。
:::

```vue
<template>
    <lay-config-provider locale="zh_CN">
        <App />
    </lay-config-provider>
</template>
```